﻿using Polenter.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace PWM_Calculator
{
    public partial class Form1 : Form
    {

        private static string configPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "PWMCalc");
        private static string confFile = "PWMCalc.conf";
        private Dictionary<string, Preset> presets = new Dictionary<string, Preset>();
        Preset currentPreset;

        public Form1()
        {
            InitializeComponent();

            string xmlFile = Path.Combine(configPath, confFile);
            var serializer = new SharpSerializer();

            if (File.Exists(xmlFile))
                presets = (Dictionary<string, Preset>)serializer.Deserialize(xmlFile);
            else
                presets = new Dictionary<string, Preset>();


            reloadList();
        }

        private void btnAddPreset_Click(object sender, EventArgs e)
        {
            Preset p = new Preset();
            p.Name = "Presets #" + presets.Count().ToString();
            presets.Add(p.Name, p);
            lb_Presets.Items.Add(p.Name);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveCurrent();
        }

        private void saveCurrent()
        {
            if (currentPreset == null)
                return;

            string oldName = currentPreset.Name;
            currentPreset.Name = tb_Name.Text;
            currentPreset.ControllerHz = (int)nud_ControllerHz.Value;
            currentPreset.PWMFreqHz = (int)nud_PWMFreq.Value;
            currentPreset.PER = (int)nud_PER.Value;
            currentPreset.CCMS1 = nud_CCMS1.Value;
            currentPreset.CCMS2 = nud_CCMS2.Value;
            currentPreset.Factor = (int)nud_Factor.Value;
            currentPreset.chartArea = (int)nud_ChartDuration.Value;

            if (rb_PER.Checked)
                currentPreset.Mode = 1;
            else
                currentPreset.Mode = 2;

            if (tb_Name.Text != oldName)
            {
                presets.Remove(oldName);
                presets.Add(currentPreset.Name, currentPreset);
                reloadList();
            }
            writeData();
        }

        private void reloadList()
        {
            int sIndex = lb_Presets.SelectedIndex;

            lb_Presets.Items.Clear();
            foreach (KeyValuePair<string, Preset> kvp in presets)
            {
                lb_Presets.Items.Add(kvp.Key);
            }

            if (lb_Presets.Items.Count > sIndex)
                lb_Presets.SelectedIndex = sIndex;
        }

        private void lb_Presets_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = lb_Presets.SelectedItem.ToString();
            currentPreset = presets[name];

            if (currentPreset.Mode == 1)
                rb_PER.Checked = true;
            else
                rb_Freq.Checked = true;

            tb_Name.Text = currentPreset.Name;
            nud_ControllerHz.Value = currentPreset.ControllerHz;
            nud_PWMFreq.Value = currentPreset.PWMFreqHz;
            nud_PER.Value = currentPreset.PER;
            nud_CCMS1.Value = currentPreset.CCMS1;
            nud_CCMS2.Value = currentPreset.CCMS2;
            nud_Factor.Value = currentPreset.Factor;
            nud_ChartDuration.Value = currentPreset.chartArea;

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            writeData();
        }

        private void writeData()
        {

            if (!Directory.Exists(configPath))
                Directory.CreateDirectory(configPath);

            var serializer = new SharpSerializer();
            serializer.Serialize(presets, Path.Combine(configPath, confFile));
        }

        private void btn_Calc_Click(object sender, EventArgs e)
        {
            updateChart();
        }

        private void rb_PER_CheckedChanged(object sender, EventArgs e)
        {
            modeChanged();
        }

        private void rb_Freq_CheckedChanged(object sender, EventArgs e)
        {
            modeChanged();

        }

        private void modeChanged()
        {
            if (rb_Freq.Checked)
            {
                nud_PER.Enabled = true;
                nud_PWMFreq.Enabled = false;
            }
            else
            {
                nud_PER.Enabled = false;
                nud_PWMFreq.Enabled = true;
            }
        }

        private void nud_ControllerHz_ValueChanged(object sender, EventArgs e)
        {
            updatePWMHz();
        }

        private void nud_Factor_ValueChanged(object sender, EventArgs e)
        {
            updatePWMHz();
        }

        private void updatePWMHz()
        {
            try
            {
                nudPWMHz.Value = nud_ControllerHz.Value / nud_Factor.Value;
            } catch (Exception ex){

            }
        }

        private void nudPWMHz_ValueChanged(object sender, EventArgs e)
        {
            if (rb_PER.Checked)
                updatePER();
        }

        private void nud_PWMFreq_ValueChanged(object sender, EventArgs e)
        {
            nud_Duration.Value = (1 / nud_PWMFreq.Value)*1000;


            if (rb_PER.Checked)
                updatePER();
        }

        private void updatePER()
        {
            try
            {
                nud_PER.Value = (nudPWMHz.Value / nud_PWMFreq.Value)-1;
            }
            catch (Exception ex)
            {

            }
        }

        private void nud_PER_ValueChanged(object sender, EventArgs e)
        {
            if (rb_Freq.Checked)
            {
                updateFreq();
            }
            else
            {
                try
                {
                    nud_resolution.Value = (decimal)(Math.Log((double)(nud_PER.Value + 1)) / Math.Log(2));
                    updateCCx();
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void updateFreq()
        {
            nud_PWMFreq.Value = nud_ControllerHz.Value / (nud_Factor.Value * nud_PER.Value + 1);
        }

        private void nud_CCMS1_ValueChanged(object sender, EventArgs e)
        {
            updateCCx();
        }

        private void nud_CCMS2_ValueChanged(object sender, EventArgs e)
        {
            updateCCx();
        }

        private void updateCCx()
        {

            try
            {
                nud_CC2.Value = (nud_PER.Value / nud_Duration.Value) * nud_CCMS2.Value;
                updateChart();
            }
            catch (Exception ex)
            {

            }

            try
            {
                nud_CC1.Value = (nud_PER.Value / nud_Duration.Value) * nud_CCMS1.Value;
                updateChart();
            }
            catch (Exception ex)
            {

            }
        }

        private void updateChart()
        {
            int chartMax = (int)nud_ChartDuration.Value;
            int chartYMax = (int)nud_PER.Value;

            chart1.ChartAreas[0].AxisX.Minimum = 0;
            chart1.ChartAreas[0].AxisX.Maximum = chartMax;

            chart1.ChartAreas[0].AxisY.Minimum = 0;
            chart1.ChartAreas[0].AxisY.Maximum = chartYMax;

            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart1.Series[2].Points.Clear();


            int c = 0;
            int y = 0;
            chart1.Series[0].Points.AddXY(0, 0);
            c += (int)nud_Duration.Value;
            while (c <= chartMax)
            {
                chart1.Series[0].Points.AddXY(c, chartYMax);
                chart1.Series[0].Points.AddXY(c, 0);

                c += (int)nud_Duration.Value;
            }

            if (nud_CCMS1.Value != 0)
            {
                chartYMax = (int)nud_CC1.Value;
                chart1.Series[1].Points.AddXY(0, chartYMax);
                decimal nextUpdate = nud_Duration.Value;
                decimal nextMatch = nud_CCMS1.Value;
                while (nextMatch <= chartMax && nextUpdate <= chartMax)
                {

                    chart1.Series[1].Points.AddXY(nextMatch, chartYMax);
                    chart1.Series[1].Points.AddXY(nextMatch, 0);

                    chart1.Series[1].Points.AddXY(nextUpdate, 0);
                    chart1.Series[1].Points.AddXY(nextUpdate, chartYMax);

                    nextMatch += nud_Duration.Value;
                    nextUpdate += nud_Duration.Value;
                }
            }


            if (nud_CCMS2.Value != 0)
            {
                chartYMax = (int)nud_CC2.Value;
                chart1.Series[2].Points.AddXY(0, chartYMax);
                decimal nextUpdate = nud_Duration.Value;
                decimal nextMatch = nud_CCMS2.Value;
                while (nextMatch <= chartMax && nextUpdate <= chartMax)
                {

                    chart1.Series[2].Points.AddXY(nextMatch, chartYMax);
                    chart1.Series[2].Points.AddXY(nextMatch, 0);

                    chart1.Series[2].Points.AddXY(nextUpdate, 0);
                    chart1.Series[2].Points.AddXY(nextUpdate, chartYMax);

                    nextMatch += nud_Duration.Value;
                    nextUpdate += nud_Duration.Value;
                }
            }
        }

        private void nud_ChartDuration_ValueChanged(object sender, EventArgs e)
        {

            updateChart();
        }

        private void nud_Duration_ValueChanged(object sender, EventArgs e)
        {
            nud_CCMS1.Maximum = nud_Duration.Value;
            nud_CCMS2.Maximum = nud_Duration.Value;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            string name = lb_Presets.SelectedItem.ToString();
            presets.Remove(name);
            reloadList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
