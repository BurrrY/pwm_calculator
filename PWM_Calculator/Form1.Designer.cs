﻿namespace PWM_Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.lb_Presets = new System.Windows.Forms.ListBox();
            this.btnAddPreset = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.nud_ControllerHz = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.nud_ChartDuration = new System.Windows.Forms.NumericUpDown();
            this.tb_Name = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.gp_Mode = new System.Windows.Forms.GroupBox();
            this.rb_Freq = new System.Windows.Forms.RadioButton();
            this.rb_PER = new System.Windows.Forms.RadioButton();
            this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            this.nud_CCD = new System.Windows.Forms.NumericUpDown();
            this.nud_CCMS2 = new System.Windows.Forms.NumericUpDown();
            this.nud_CC2 = new System.Windows.Forms.NumericUpDown();
            this.nud_CCMS1 = new System.Windows.Forms.NumericUpDown();
            this.nud_CC1 = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nud_Duration = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nud_PER = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nud_PWMFreq = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nud_resolution = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nudPWMHz = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.nud_Factor = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ControllerHz)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ChartDuration)).BeginInit();
            this.gp_Mode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CCMS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CCMS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Duration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_PER)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_PWMFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_resolution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPWMHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Factor)).BeginInit();
            this.SuspendLayout();
            // 
            // lb_Presets
            // 
            this.lb_Presets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_Presets.FormattingEnabled = true;
            this.lb_Presets.Location = new System.Drawing.Point(12, 12);
            this.lb_Presets.Name = "lb_Presets";
            this.lb_Presets.Size = new System.Drawing.Size(179, 394);
            this.lb_Presets.TabIndex = 0;
            this.lb_Presets.SelectedIndexChanged += new System.EventHandler(this.lb_Presets_SelectedIndexChanged);
            // 
            // btnAddPreset
            // 
            this.btnAddPreset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddPreset.Location = new System.Drawing.Point(12, 422);
            this.btnAddPreset.Name = "btnAddPreset";
            this.btnAddPreset.Size = new System.Drawing.Size(75, 23);
            this.btnAddPreset.TabIndex = 1;
            this.btnAddPreset.Text = "+";
            this.btnAddPreset.UseVisualStyleBackColor = true;
            this.btnAddPreset.Click += new System.EventHandler(this.btnAddPreset_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(116, 422);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "-";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(197, 12);
            this.chart1.Name = "chart1";
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Name = "Counter";
            series2.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.WideUpwardDiagonal;
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series2.Color = System.Drawing.Color.Lime;
            series2.Name = "Output1";
            series3.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.WideDownwardDiagonal;
            series3.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            series3.BorderWidth = 3;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series3.Color = System.Drawing.Color.Red;
            series3.Name = "Output2";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(785, 189);
            this.chart1.TabIndex = 3;
            this.chart1.Text = "chart1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Controller-Takt (Hz):";
            // 
            // nud_ControllerHz
            // 
            this.nud_ControllerHz.Location = new System.Drawing.Point(27, 73);
            this.nud_ControllerHz.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nud_ControllerHz.Name = "nud_ControllerHz";
            this.nud_ControllerHz.Size = new System.Drawing.Size(120, 20);
            this.nud_ControllerHz.TabIndex = 5;
            this.nud_ControllerHz.ThousandsSeparator = true;
            this.nud_ControllerHz.ValueChanged += new System.EventHandler(this.nud_ControllerHz_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.tb_Name);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.gp_Mode);
            this.groupBox1.Controls.Add(this.numericUpDown9);
            this.groupBox1.Controls.Add(this.nud_CCD);
            this.groupBox1.Controls.Add(this.nud_CCMS2);
            this.groupBox1.Controls.Add(this.nud_CC2);
            this.groupBox1.Controls.Add(this.nud_CCMS1);
            this.groupBox1.Controls.Add(this.nud_CC1);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.nud_Duration);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.nud_PER);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.nud_PWMFreq);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.nud_resolution);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.nudPWMHz);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.nud_Factor);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nud_ControllerHz);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(197, 207);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(785, 238);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Location = new System.Drawing.Point(322, 102);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(135, 69);
            this.groupBox3.TabIndex = 31;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Modus";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 42);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(72, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.Text = "Dualslope";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(79, 17);
            this.radioButton2.TabIndex = 0;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Singleslope";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(463, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "Match bei                 ms:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.nud_ChartDuration);
            this.groupBox2.Location = new System.Drawing.Point(16, 182);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(581, 50);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Diagramm";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "Anzeigedauer (ms):";
            // 
            // nud_ChartDuration
            // 
            this.nud_ChartDuration.Location = new System.Drawing.Point(111, 19);
            this.nud_ChartDuration.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nud_ChartDuration.Name = "nud_ChartDuration";
            this.nud_ChartDuration.Size = new System.Drawing.Size(120, 20);
            this.nud_ChartDuration.TabIndex = 32;
            this.nud_ChartDuration.ThousandsSeparator = true;
            this.nud_ChartDuration.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nud_ChartDuration.ValueChanged += new System.EventHandler(this.nud_ChartDuration_ValueChanged);
            // 
            // tb_Name
            // 
            this.tb_Name.Location = new System.Drawing.Point(27, 32);
            this.tb_Name.Name = "tb_Name";
            this.tb_Name.Size = new System.Drawing.Size(120, 20);
            this.tb_Name.TabIndex = 34;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 33;
            this.label11.Text = "Bezeichnung:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(522, 152);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 32;
            this.btnSave.Text = "Speichern";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gp_Mode
            // 
            this.gp_Mode.Controls.Add(this.rb_Freq);
            this.gp_Mode.Controls.Add(this.rb_PER);
            this.gp_Mode.Location = new System.Drawing.Point(178, 102);
            this.gp_Mode.Name = "gp_Mode";
            this.gp_Mode.Size = new System.Drawing.Size(135, 69);
            this.gp_Mode.TabIndex = 30;
            this.gp_Mode.TabStop = false;
            this.gp_Mode.Text = "Modus";
            // 
            // rb_Freq
            // 
            this.rb_Freq.AutoSize = true;
            this.rb_Freq.Location = new System.Drawing.Point(6, 42);
            this.rb_Freq.Name = "rb_Freq";
            this.rb_Freq.Size = new System.Drawing.Size(123, 17);
            this.rb_Freq.TabIndex = 1;
            this.rb_Freq.Text = "Frequenz berechnen";
            this.rb_Freq.UseVisualStyleBackColor = true;
            this.rb_Freq.CheckedChanged += new System.EventHandler(this.rb_Freq_CheckedChanged);
            // 
            // rb_PER
            // 
            this.rb_PER.AutoSize = true;
            this.rb_PER.Checked = true;
            this.rb_PER.Location = new System.Drawing.Point(6, 19);
            this.rb_PER.Name = "rb_PER";
            this.rb_PER.Size = new System.Drawing.Size(101, 17);
            this.rb_PER.TabIndex = 0;
            this.rb_PER.TabStop = true;
            this.rb_PER.Text = "PER berechnen";
            this.rb_PER.UseVisualStyleBackColor = true;
            this.rb_PER.CheckedChanged += new System.EventHandler(this.rb_PER_CheckedChanged);
            // 
            // numericUpDown9
            // 
            this.numericUpDown9.Location = new System.Drawing.Point(552, 114);
            this.numericUpDown9.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown9.Name = "numericUpDown9";
            this.numericUpDown9.ReadOnly = true;
            this.numericUpDown9.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown9.TabIndex = 29;
            // 
            // nud_CCD
            // 
            this.nud_CCD.Location = new System.Drawing.Point(472, 114);
            this.nud_CCD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nud_CCD.Name = "nud_CCD";
            this.nud_CCD.ReadOnly = true;
            this.nud_CCD.Size = new System.Drawing.Size(69, 20);
            this.nud_CCD.TabIndex = 28;
            this.nud_CCD.ThousandsSeparator = true;
            // 
            // nud_CCMS2
            // 
            this.nud_CCMS2.DecimalPlaces = 1;
            this.nud_CCMS2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nud_CCMS2.Location = new System.Drawing.Point(552, 73);
            this.nud_CCMS2.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nud_CCMS2.Name = "nud_CCMS2";
            this.nud_CCMS2.Size = new System.Drawing.Size(45, 20);
            this.nud_CCMS2.TabIndex = 27;
            this.nud_CCMS2.ValueChanged += new System.EventHandler(this.nud_CCMS2_ValueChanged);
            // 
            // nud_CC2
            // 
            this.nud_CC2.Location = new System.Drawing.Point(472, 73);
            this.nud_CC2.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nud_CC2.Name = "nud_CC2";
            this.nud_CC2.ReadOnly = true;
            this.nud_CC2.Size = new System.Drawing.Size(69, 20);
            this.nud_CC2.TabIndex = 26;
            this.nud_CC2.ThousandsSeparator = true;
            // 
            // nud_CCMS1
            // 
            this.nud_CCMS1.DecimalPlaces = 1;
            this.nud_CCMS1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nud_CCMS1.Location = new System.Drawing.Point(552, 32);
            this.nud_CCMS1.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nud_CCMS1.Name = "nud_CCMS1";
            this.nud_CCMS1.Size = new System.Drawing.Size(45, 20);
            this.nud_CCMS1.TabIndex = 24;
            this.nud_CCMS1.ValueChanged += new System.EventHandler(this.nud_CCMS1_ValueChanged);
            // 
            // nud_CC1
            // 
            this.nud_CC1.Location = new System.Drawing.Point(472, 32);
            this.nud_CC1.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nud_CC1.Name = "nud_CC1";
            this.nud_CC1.ReadOnly = true;
            this.nud_CC1.Size = new System.Drawing.Size(69, 20);
            this.nud_CC1.TabIndex = 23;
            this.nud_CC1.ThousandsSeparator = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(463, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Match bei                 ms:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(463, 99);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Differenz:";
            // 
            // nud_Duration
            // 
            this.nud_Duration.DecimalPlaces = 1;
            this.nud_Duration.Location = new System.Drawing.Point(329, 73);
            this.nud_Duration.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nud_Duration.Name = "nud_Duration";
            this.nud_Duration.ReadOnly = true;
            this.nud_Duration.Size = new System.Drawing.Size(120, 20);
            this.nud_Duration.TabIndex = 17;
            this.nud_Duration.ThousandsSeparator = true;
            this.nud_Duration.ValueChanged += new System.EventHandler(this.nud_Duration_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(315, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Periodendauer (ms):";
            // 
            // nud_PER
            // 
            this.nud_PER.Enabled = false;
            this.nud_PER.Location = new System.Drawing.Point(329, 32);
            this.nud_PER.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nud_PER.Name = "nud_PER";
            this.nud_PER.Size = new System.Drawing.Size(120, 20);
            this.nud_PER.TabIndex = 15;
            this.nud_PER.ThousandsSeparator = true;
            this.nud_PER.ValueChanged += new System.EventHandler(this.nud_PER_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(315, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "PER";
            // 
            // nud_PWMFreq
            // 
            this.nud_PWMFreq.Location = new System.Drawing.Point(184, 73);
            this.nud_PWMFreq.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nud_PWMFreq.Name = "nud_PWMFreq";
            this.nud_PWMFreq.Size = new System.Drawing.Size(120, 20);
            this.nud_PWMFreq.TabIndex = 13;
            this.nud_PWMFreq.ThousandsSeparator = true;
            this.nud_PWMFreq.ValueChanged += new System.EventHandler(this.nud_PWMFreq_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(170, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "PWM-Frequenz (Hz):";
            // 
            // nud_resolution
            // 
            this.nud_resolution.Location = new System.Drawing.Point(184, 32);
            this.nud_resolution.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nud_resolution.Name = "nud_resolution";
            this.nud_resolution.ReadOnly = true;
            this.nud_resolution.Size = new System.Drawing.Size(120, 20);
            this.nud_resolution.TabIndex = 11;
            this.nud_resolution.ThousandsSeparator = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(170, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Auflösung (bit)";
            // 
            // nudPWMHz
            // 
            this.nudPWMHz.Location = new System.Drawing.Point(27, 155);
            this.nudPWMHz.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nudPWMHz.Name = "nudPWMHz";
            this.nudPWMHz.ReadOnly = true;
            this.nudPWMHz.Size = new System.Drawing.Size(120, 20);
            this.nudPWMHz.TabIndex = 9;
            this.nudPWMHz.ThousandsSeparator = true;
            this.nudPWMHz.ValueChanged += new System.EventHandler(this.nudPWMHz_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Timer-Takt (Hz):";
            // 
            // nud_Factor
            // 
            this.nud_Factor.Location = new System.Drawing.Point(27, 114);
            this.nud_Factor.Name = "nud_Factor";
            this.nud_Factor.Size = new System.Drawing.Size(120, 20);
            this.nud_Factor.TabIndex = 7;
            this.nud_Factor.ThousandsSeparator = true;
            this.nud_Factor.ValueChanged += new System.EventHandler(this.nud_Factor_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Teiler:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 461);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnAddPreset);
            this.Controls.Add(this.lb_Presets);
            this.Name = "Form1";
            this.Text = "PWM-Calc 1.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ControllerHz)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ChartDuration)).EndInit();
            this.gp_Mode.ResumeLayout(false);
            this.gp_Mode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CCMS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CCMS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_CC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Duration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_PER)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_PWMFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_resolution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPWMHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Factor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lb_Presets;
        private System.Windows.Forms.Button btnAddPreset;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nud_ControllerHz;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nud_CC1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nud_Duration;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nud_PER;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nud_PWMFreq;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nud_resolution;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudPWMHz;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nud_Factor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown9;
        private System.Windows.Forms.NumericUpDown nud_CCMS2;
        private System.Windows.Forms.NumericUpDown nud_CC2;
        private System.Windows.Forms.NumericUpDown nud_CCMS1;
        private System.Windows.Forms.GroupBox gp_Mode;
        private System.Windows.Forms.RadioButton rb_Freq;
        private System.Windows.Forms.RadioButton rb_PER;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_Name;
        private System.Windows.Forms.NumericUpDown nud_CCD;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nud_ChartDuration;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
    }
}

