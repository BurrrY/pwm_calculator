﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWM_Calculator
{
    class Preset
    {
        public int ControllerHz { get; set; }

        public string Name { get; set; }

        public int PWMFreqHz { get; set; }

        public int PER { get; set; }

        public decimal CCMS1 { get; set; }

        public decimal CCMS2 { get; set; }

        public int Mode { get; set; }


        public Preset()
        {

            ControllerHz = 32000000;
            Name = "neues Preset";
            PWMFreqHz = 200;
            PER = 2499;
            CCMS1 = 0;
            CCMS2 = 0;
            Mode = 0;
            Factor = 4;
        }

        public int Factor { get; set; }

        public int chartArea { get; set; }
    }
}
